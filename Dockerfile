FROM alpine
RUN apk update
RUN apk add openssh lftp
RUN  echo 'set sftp:connect-program "ssh -a -x -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"' > $HOME/.lftprc
